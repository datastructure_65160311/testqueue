public class QueueApp2 {
    public static void main(String[] args) {
        Queue2 q = new Queue2(5);
        q.insert(1);
        q.insert(2);
        q.insert(3);
        q.insert(4);
        q.insert(5);
        q.insert(6);
        System.out.println(q.peek());
        System.out.println(q.size());
        q.insert(7);
        System.out.println(q.size());
        System.out.println(q.isEmpty());
        System.out.println(q.isFull());

    }
}
